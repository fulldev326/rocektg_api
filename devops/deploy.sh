#!/usr/bin/env bash
ssh -o StrictHostKeyChecking=no root@89.187.161.145 << 'ENDSSH'
 cd /rocketg_api
 docker login -u $REGISTRY_USER -p $CI_BUILD_TOKEN $CI_REGISTRY
 docker pull registry.gitlab.com/fulldev326/rocketg_api:latest
 docker-compose up -d
ENDSSH
